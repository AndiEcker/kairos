""" member meetings model """
from django.conf import settings
from django.db import models

from kairos.utils import member_full_name


class MemberMeeting(models.Model):
    """ member meetings model """
    mt_author = models.ForeignKey(settings.AUTH_USER_MODEL, on_delete=models.CASCADE)
    mt_language = models.CharField(max_length=3, choices=settings.LANGUAGES)
    mt_text = models.TextField()
    mt_created = models.DateTimeField(auto_now_add=True)

    def __str__(self):
        # noinspection PyUnresolvedReferences
        return (f"{self.__class__.__name__}-{self.mt_created}={self.mt_language}"
                f"@{member_full_name(self.mt_author)}"
                f":{self.mt_text}")
