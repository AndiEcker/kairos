""" view to add a new member meeting text. """
from cms.utils.permissions import get_current_user
from django.conf import settings
from django.shortcuts import redirect
from django.utils.html import strip_tags
from django.utils.translation import override
from django.views import View

from ae.django_utils import requested_language

from mbr_meeting_plugin.models import MemberMeeting
from kairos.utils import send_mbr_notif


MBR_MEET_ANCHOR_ID_PREFIX = 'MMeetAIdP'


class MemberMeetingAddView(View):
    """ add new member meeting with text block, language, author, created date. """
    @staticmethod
    def post(request, *args, **kwargs):
        """ form post to add new member meeting text into the database. """
        updated_languages = []
        for lang, _lang_name in settings.LANGUAGES:
            updated_text = request.POST.get(lang + '_text')
            if updated_text != request.POST.get(lang + '_old_text'):
                MemberMeeting.objects.create(
                    mt_author=get_current_user(),
                    mt_language=lang,
                    mt_text=updated_text,
                )
                updated_languages.append(lang)

            if request.POST.get(lang + '_notify'):
                with override(lang):
                    send_mbr_notif('meeting', [strip_tags(updated_text)])   # strip tags for Telegram

        return redirect(request.META.get('HTTP_REFERER', '/') + '#' + MBR_MEET_ANCHOR_ID_PREFIX +
                        (updated_languages and updated_languages[0] or requested_language()))
