""" member meetings plugin """
from django.conf import settings

from cms.models import CMSPlugin
from cms.plugin_base import CMSPluginBase
from cms.plugin_pool import plugin_pool

from ae.django_utils import requested_language

from mbr_meeting_plugin.models import MemberMeeting
from mbr_meeting_plugin.views import MBR_MEET_ANCHOR_ID_PREFIX


@plugin_pool.register_plugin
class MemberMeetingsPlugin(CMSPluginBase):
    """ member meetings plug-in """
    model = CMSPlugin
    render_template = "membermeetings_plugin.html"
    cache = False

    def render(self, context, instance, placeholder):
        """ add anchor/lang prefix and meeting texts in all available languages onto context. """
        context = super().render(context, instance, placeholder)

        context['MBR_MEET_ANCHOR_ID_PREFIX'] = MBR_MEET_ANCHOR_ID_PREFIX
        context['mt_texts'] = mtt = {}
        for lang, _lang_name in settings.LANGUAGES:
            mts = MemberMeeting.objects.filter(mt_language=lang).order_by('-mt_created').first()
            mtt[lang] = mts.mt_text if mts else ""
        context['mt_lang_text'] = mtt[requested_language()]

        return context
