""" Generic Django CMS website project for sharing and announcement associations


Installation/Setup
==================

Django CMS 3.8
--------------

inspired by youtube video tutorial https://www.youtube.com/watch?v=NbsRVfLCE1U

* on a new OS/system ensure the system/apt/non-python libs needed by Django CMS are installed (see
  https://djangocms-installer.readthedocs.io/en/latest/libraries.html).
* create a new python virtual environment (web39) and activate it
* pip install djangocms-installer (version 2.0)
* cd to your projects parent folder then run:
* djangocms --permissions=yes --languages=en,es,de --bootstrap=yes --parent-dir=kairos kairos
* cd kairos
* pyenv local web39
* open the project in PyCharm and then:
* set the projects python interpreter to the virtual env (web39)
* edit requirements.txt downgrading the version of django-treebeard to 4.4 to prevent an exception on publishing
  (VeshRaazThapa recommends even using 4.3.1 instead of 4.4 in https://github.com/django-cms/django-cms/issues/6980)
* open kairos/settings.py and to (1) fix the 4 inspections “Missed locally stored library for HTTP link” by downloading
  the external libraries and (2) reformat long lines with the hot-fix “Put attributes on separate lines”
* open kairos/templates/base.html and replace the tag <html> with <html lang="{{ LANGUAGE_CODE }}">
* open kairos/settings.py and replace in CMS_TEMPLATES feature/fullwidth/home/page with ('base.html', 'Default'),
* in kairos/templates delete feature/fullwidth/home/page.html


Reset database and migrations
=============================

inspired by https://stackoverflow.com/questions/34576004 and
https://simpleisbetterthancomplex.com/tutorial/2016/07/26/how-to-reset-migrations.html

* delete migration files by running the following 2 commands in shell:
    find . -path "*/migrations/*.py" -not -name "__init__.py" -delete
    find . -path "*/migrations/*.pyc"  -delete
* drop your Sqlite database (with rm project.db for django cms projects)
* create the initial migrations and generate the database schema by running these 2 commands:
    python manage.py makemigrations
    python manage.py migrate
* create supervisor by running:
    python manage.py createsuperuser
* run the server and in example.com/Administration/DJANGO CMS/User Groups(Page) create a new group 'members', then
  uncheck/remove page "add" and "update/edit" privileges defaults. finally either add to this group the permissions to
  add/change/delete member announcements and messages (or split/pass them to another/new group if you want to allow
  only some users to post messages and announcements).
* stop the server
* run the script kairos/management/commands/_members_and_announcements_data_loader.py.
* run ./manage.py generate_thumbnails (not sure on that, because they are not displayed in filer admin).
* run the server again and start adding the CMS pages.


Deploy on pythonanywhere
========================

initial deployment
------------------

* add static and media folders to repo by running locally:
 - git add -f media
 - git add -f static
 (if you see warning on CRLF/LF conversions then correct them conversion e.g. with: git add -f -u --renormalize static)
* log into your pythonanywhere account and then open bash console to run:
 - git clone https://gitlab.com/AndiEcker/kairos
 - cd kairos
 - mkvirtualenv --python=/usr/bin/python3.9 kairos-web39
 - pip install -r requirements_221127.txt
* create new app and WSGI file (see "Setting up your Web app and WSGI file" in
  https://help.pythonanywhere.com/pages/DeployExistingDjangoProject/)
* upload/create a .env file to/in hosts home folder (~ or /home/AndiEcker) containing the env vars for django (DJANGO_*)
  the change notification settings (MCN_*) and the data import (MEMBERS_DATA_FILE_PATH, ...).
* add at the top of your WSGI file (available via the Web tab) the following lines
  (see also https://help.pythonanywhere.com/pages/environment-variables-for-web-apps/):
    from dotenv import load_dotenv
    load_dotenv()
* add to the virtualenv script at ~/.virtualenvs/kairos-web39/bin/postactivate the following line (and Save it):
    set -a; source ~/.env; set +a
* correct/complete ~/.bashrc with:
    HISTCONTROL=ignorespace:erasedups
    shopt -s histappend
    HISTSIZE=3000
    HISTFILESIZE=6000


switch domain from siteground to pythonanywhere host
====================================================

* login into siteground



update deployment
-----------------

* update develop branch on github by running:
 grm -b <branch> renew & grm prepare & grm commit & grm push & grm -f -u AndiEcker request & grm release LATEST
* open bash console and run from the home directory (home/AndiEcker):
 - rm -r kairos
 - git clone https://gitlab.com/AndiEcker/kairos
* go to the Web tab in pythonanywhere and reload the app (especially if you updated the gettext translation .mo files).


DIGI zyxel router dyndns options
================================

- dyndns.com
- dtdns.com
- no-ip.com
- easydns.com
- freedns.afraid.org
- tzo.com


TODO:
=====
- (PyCharm bug) register show_menu from menus lib to fix "Unresolved tag" inspection warning in PyCharm IDE:
    * tried adding 'builtins': ["menus.templatetags.menu_tags"] to settings.TEMPLATES[0]['OPTIONS']
    * tried adding the following code to kairos.__init__.py/.settings.py/.asgi.py/.wsgi.py/.url.py/...
        # register show_menu from menus lib to fix "Unresolved tag" inspection warning in PyCharm IDE
        from django import template
        from menus.templatetags.menu_tags import ShowMenu
        register = template.Library()
        register.tag('show_menu', ShowMenu)
    * tried adding 'menus.templatetags', to settings.INSTALLED_APPS
    * one workaround was to add the tag name 'show_menu' into the register.tag(ShowMenu) call as 1st argument
      into menus.template_tags.menu_tags.py line 173
- (PyCharm bug) fix wrong inspection "Failed to resolve url..." in url templatetags, e.g. {% url 'login' %} in
   kairos/templates/registration/login.html
- (PyCharm bug) "Closing tag..." inspection if forloop.last is used (e.g. memberannouncement_list.html)
- (PyCharm bug) {% include %} shows warning "Unresolved template reference..." although in same folder
  (memberannouncement)
- (PyCharm bug) inspection PyUnresolvedReferences on self.ma_user.username in mbr_announcements/models.py
- fix cms_toolbar/navbar overlap - the following workarounds don't work:
    <style>
        // prevent overlap of cms_toolbar and navbar from https://stackoverflow.com/questions/32027903
        .cms-toolbar-expanded body, .cms-toolbar-expanded .navbar-fixed-top {
            top: 30px;
        }
        // prevent cms_toolbar/navbar overlap https://github.com/django-cms/django-cms/issues/6242 & 6392
        // .. cms-toolbar-expanded class get added as class to html element when the cms toolbar is visible
        html.cms-toolbar-expanded {
            .navbar-fixed-top, .fixed-top {
                top: 46px;
            }
            .modal-dialog {
                margin-top: 3.5rem;
            }
        }
    </style>

    // not tried using javascript:
    if(document.getElementById("html").classList.contains("cms-toolbar-expanded")){
        // cms toolbar is visible, so but now I set/change the "*top" css using js
    }
- make spacing of content nicer to not get overlapped by navbar (currently using 4 div row elements in base.html)

"""

__version__ = '0.0.76'
