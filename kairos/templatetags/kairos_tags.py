""" add flag icons to your templates. """
from django import template
from django.contrib.auth import get_user_model

from ae.django_utils import generic_language

from kairos.utils import member_full_name


register = template.Library()


@register.simple_tag
def flag_icon(language: str) -> str:
    """ return css classes to specify a flag icon for the passed language code.

    :param language:            language code (ignoring country specifiers, after a hyphen).
    :return:                    flag-icon classes for the specified language code.
    """
    language = generic_language(language)
    return f"flag-icon flag-icon-{'gb' if language == 'en' else language}"


@register.simple_tag
def full_name(member_rec_or_pk) -> str:
    """ return the full and unique name of a member.

    :param member_rec_or_pk:    member user record or the primary key of the user record.
    :return:                    full name of the member.
    """
    if isinstance(member_rec_or_pk, int):
        member_rec_or_pk = get_user_model().objects.get(pk=member_rec_or_pk)
    return member_full_name(member_rec_or_pk)
