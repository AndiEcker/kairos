""" ordering and selection of cms navbar menu items, displayed by the show_menu template tag. """
# modified / commented-out the following imports after migration of members-list to a pseudo CMS-page
# from django.urls import reverse
# from django.utils.translation import gettext as _
# from menus.base import Menu, Modifier, NavigationNode
from menus.base import Modifier
from menus.menu_pool import menu_pool


def _select_node(nodes, slug):
    for node in nodes:
        if node.namespace == 'CMSMenu' and node.path == slug:   # only cms menu nodes provide a path attribute
            node.selected = True
            break


@menu_pool.register_modifier
class SelectionAndOrdering(Modifier):
    """ modify menu items/nodes in order to select sub-urls and to sort/order them. """
    def modify(self, request, nodes, namespace, root_id, post_cut, breadcrumb):
        """ select and order menu items by their reverse-id (for cms pages) or id (for nodes added via get_nodes()). """
        if post_cut:
            # needed only if members-list page get added via MemberMenu.get_nodes(): _hide_member_list(nodes)

            path = request.path
            if '/messages/' in path:
                _select_node(nodes, 'messages')
            elif '/announcements/' in path:
                _select_node(nodes, 'announcements')

            nodes = [node for node in sorted(nodes, key=lambda _: int(_.attr.get('reverse_id') or _.id))]

        return nodes


# alternative way to hide members list menu if added via MemberMenu.get_nodes()
# def _hide_member_list(nodes):
#     for node in nodes:
#         if node.namespace == 'CMSMenu' and node.path == 'members-list':
#             node.attr['visible_for_anonymous'] = False
#
#
# alternative way to add non-CMS page members-list to the menu
# @menu_pool.register_menu
# class MemberMenu(Menu):
#     """ extend menus in the navigation bar for dynamic pages and other extension. """
#     def get_nodes(self, request):
#         """ add members-list menu (because hide-for-anonymous is not working in Modifier). """
#         return [NavigationNode(_("Members"), reverse('members-list'), 999, attr={'visible_for_anonymous': False})]
