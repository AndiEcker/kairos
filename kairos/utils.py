""" helpers to show member name and to process notifications on change of member messages/announcements. """
import os

from typing import Iterable, Optional

from cms.utils.permissions import get_current_user
from django.contrib.auth.models import User
from django.utils.translation import gettext as _

from ae.notify import Notifications


def member_full_name(member_rec) -> str:
    """ return the full and unique display-name of a member.

    :param member_rec:          member user record.
    :return:                    unique member name in the format username-first_name.
    """
    return _("{member_id}-{name}").format(member_id=member_rec.username, name=member_rec.first_name)


# get optional MCN_* (MemberChangesNotifications) service config env vars, loaded by settings.py if not in exported
CHANGE_RECEIVERS = os.environ.get(
    "MCN_RECEIVERS", "mailto:aecker2@gmail.com=96-Andi,telegram:-802339565=Kairos-Group").split(',')
notification_service = Notifications(
    smtp_server_uri=os.environ.get("MCN_MAIL_URI", ""), mail_from=os.environ.get("MCN_MAIL_FROM", "website@kairos.org"),
    telegram_token=os.environ.get("MCN_TG_TOK", ""),
    whatsapp_token=os.environ.get("MCN_WA_TOK", ""), whatsapp_sender=os.environ.get("MCN_WA_FROM", ""),
)


def send_mbr_notif(mode: str, changes: Iterable[str],
                   item: str = "", author: Optional[User] = None, url: str = "", subject: Optional[str] = None):
    """ send out member notifications for meetings or changed data items.

    :param mode:                data item change type: 'added', 'updated' or 'deleted' or 'meeting' for member meetings.
    :param changes:             list/tuple of changes done to the data item / meeting text.
    :param item:                name/description of the changed item (ignored if mode == 'meeting').
    :param author:              data item author record or current user or None (to include current user as modifier).
    :param url:                 absolute url to show data item (ignored/not-used if mode in ('deleted', 'meeting')).
    :param subject:             overwrite default notification subject text.
    """
    if subject is None:
        subject = _("notification from kairos website")

    if mode == 'added':
        msg = "<a href=\"{url}\">{item}</a> of {owner} got added"
    elif mode == 'updated':
        msg = "<a href=\"{url}\">{item}</a> of {owner} got changed"
    elif mode == 'deleted':
        msg = "<i>{item}</i> of {owner} got deleted"
    elif mode == 'meeting':
        msg = "<i>Member Meeting</i> notification"
    else:
        msg = "<a href=\"{url}\">{item}</a> of {owner} got {mode}"

    username = get_current_user().username
    msg = _(msg).format(owner=member_full_name(author) if author else username, item=item, url=url, mode=mode)

    if not author or username != author.username:
        msg += " (" + _("by {admin}").format(admin=username) + ")"

    for change in changes:
        msg += f"<br><br>{change}"

    for change_receiver in CHANGE_RECEIVERS:
        err_msg = notification_service.send_notification(msg, change_receiver, subject)
        if err_msg:
            print(f"***   send_mbr_notif() error with message '{msg}' to '{change_receiver}': {err_msg}")
