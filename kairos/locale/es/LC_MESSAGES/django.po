# Translation of kairos project into Spanish.
# This file is distributed under the same license as the kairos package.
# Andi Ecker <aecker2@gmail.com>, 2022.
msgid ""
msgstr ""
"Project-Id-Version: kairos\n"
"Report-Msgid-Bugs-To: https://gitlab.com/AndiEcker/kairos/-/issues\n"
"POT-Creation-Date: 2022-10-18 12:45+0000\n"
"PO-Revision-Date: YEAR-MO-DA HO:MI+ZONE\n"
"Last-Translator: Andi Ecker <aecker2@gmail.com>\n"
"Language-Team: Spanish\n"
"Language: es\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"

#. Translators: used in base_navbar.html/memberannouncement_list.html/membermessage_list.html
msgid "KairosBrandName"
msgstr "Kairos La Gomera"

#. Translators: used in sub title of navbar
msgid "Time exchange * Goods exchange * Rental exchange"
msgstr "Intercambio de tiempo * Intercambio de mercancías * Intercambio de alquiler"

#. Translators: language switch button title, used in navbar (kairos/templates/base_navbar.html)
msgid "CurrLanguage"
msgstr "Español"
"\n haze click aquí para seleccionar otro idioma"
"\n click here to select other language"
"\n klicke hier um eine andere Sprache auszuwählen"

#. Translators: used everywhere to display a unique member name (see member_full_name())
msgid "Member name"
msgstr "Nombre De Miembro"

msgid "{member_id}-{name}"
msgstr ""

#. Translators: used in navbar menu (kairos/cms_menus.py)
msgid "Members"
msgstr "Miembros"

#. Translators: used for signup and login in kairos/views.py and kairos/templates/registration/*.html
msgid "Member Id"
msgstr "Número de miembro"

msgid "First name"
msgstr "Nombre"

msgid "Phone"
msgstr "Teléfono"

msgid "'First name' can only consist of letters, numbers, spaces or hyphens."
msgstr "'Nombre' consiste sólo en letras, dígitos, espacios y guiones."

msgid "'Phone' consists of a plus sign, a 2-digit country code, and a space, followed by digits and spaces."
msgstr "'Teléfono' consiste en un signo más, un código de país de 2 dígitos y un espacio seguido de dígitos y espacios."

msgid "authenticate with your member id, entered into the 'Username' field"
msgstr "authentificarte por la introducción de tu número de miembro en el campo 'Nombre de usuario'"

msgid "Your sign-up got initiated and your member account created..."
msgstr "El registro tuyo está iniciada y tu cuenta de miembro está creada..."

msgid "Sending verification email to {email}..."
msgstr "Enviando email de verificación a {email}..."

msgid "Member Sign-Up"
msgstr "Registro de miembro"

msgid "Email Address Verification"
msgstr "Verificación de dirección de email"

msgid "Please get in contact with us, because of the following email send error:"
msgstr "Por favor ponte en contacto con nosotros, porque se ha producido el siguente error en enviar un email:"

msgid "Please click on the confirmation link in the email we just sent to you!"
msgstr "Por favor haze click en el enlace de confirmación en el email que hemos enviado a ti!"

msgid "Invalid member ids {}!"
msgstr "Número de miembro {} inválido!"

msgid "Registration {} expired error!"
msgstr "Registración {} caducado!"

msgid "Admission Fee Payment Confirmation"
msgstr "Confirmación del recibo de la cuota de inscripción"

msgid "Hi {admin}, please first log in, then try again to confirm the receive of this admission fee payment."
msgstr "Hola {admin}, primero tienes que authentificarte para confirmar el recibo de esta cuota de inscripción."

msgid "Message-Send-Error to '{admin}' on sign-up of '{name}': {err_msg}"
msgstr "Error en enviar un mensaje a `{admin}' para el registro y pago de '{name}': {err_msg}"

msgid "Account of member {name} got already activated!"
msgstr "La cuenta del miembro {name} ya estuve activada!"

msgid "Member data duplicate error(s) with {name}:"
msgstr "Error(es) de duplicidad de datos con el miembro {name}: "

msgid "A 'warm welcome' to our new member {name}"
msgstr "Una 'cálida bienvenida' a nuestro nuevo miembro {name}"

msgid "Account activated"
msgstr "Se ha activada la cuenta de miembro"

msgid "Verification of your email address failed! Please get in contact with us."
msgstr "Verificación de la dirección de email falló! Por favor ponte en contacto con nosotros."

msgid "Your email address got confirmed."
msgstr "Tu dirección de email se confirmó."

msgid "We will contact you soon via phone or email."
msgstr "Vamos a contactarte por Teléfono o email."

msgid "Your account will get activated after receipt of the admission fee."
msgstr "Tu cuenta de miembro se va ser activada despues del pago de la cuota de inscripción."

msgid "Hello"
msgstr "Hola"

msgid "Welcome as a new member!"
msgstr "Bienvenido como miembro nuevo!"

msgid "Click here to confirm your email address."
msgstr "Haze click aqui para confirmar tu dirección de email."

msgid "Thank you for signing up."
msgstr "Muchas gracias por tu registración."

msgid "We received your member registration fee. Your member account got now activated."
msgstr "Hemos recibido tu cuota de inscripción. Tu cuenta de miembro ya está activado."

msgid "Click on this link to log-/sign-in to our website"
msgstr "Haze click aqui para entrar como miembro en nuestro sitio de web"

msgid "Important! Your username to log into our website is not your first name, but your member id which is: "
msgstr "¡Importante! En entrar no uses tu nombre como nombre de usuario, si no tu número de miembro, que es: "

msgid "Use the password you entered in the sign-up."
msgstr "Usa la contraseña que has introducido en tu registración."

msgid "A new member just registered on our website!"
msgstr "Un nuevo miembro empiezó de registrarse en nuestro sitio de web."

msgid "Get to the contact data by clicking on the above link. Here you can also activate the new account."
msgstr "Para mostrar los datos de contacto haze click el enlace de arriba. Ahí puedes también activar la nueva cuenta."

msgid "Alternatively, if you already received the admission fee payment, then only click the link below:"
msgstr "O en cuando ya has recibido la cuota de inscripción, solo haze click en el enlace siguente:"

msgid "Activate account of new member: "
msgstr "Activa la cuenta de miembro de: "

msgid "With my registration I accept the <a href=\"/data-collection/\">rules of this barter circle</a>."
msgstr "Con mi inscripción acepto <a href=\"/data-collection/\">las reglas de este círculo de trueque</a>."

msgid "I have read and taken note of <a href=\"/privacy-policy/\">the information requirements</a> according to Art."
" 13 and 14 DSGVO."
msgstr "He leído y tomado nota de <a href=\"/privacy-policy/\">los requisitos de información</a> según los artículos 13"
" y 14 de la DSGVO."

msgid "Greetings from"
msgstr "Saludos de"

# Translators: used in main menu, views and templates
# mbr_announcements/views.py:33
# mbr_announcements/templates/mbr_announcements/memberannouncement_collapse_expand_buttons.html:6+17+21
# mbr_announcements/templates/mbr_announcements/memberannouncement_list.html:4
msgid "Announcements"
msgstr "Anuncios"

#. Translators: used in memberannouncement_plugin.html
# original is msgid add {{ ma_act }} in {{ cat_name }}
msgid "add %(ma_act)s in %(cat_name)s"
msgstr "añadir una nueva acción de '%(ma_act)s' a la categoría '%(cat_name)s'"

# original is msgid delete {{ ma_act }} in {{ cat_name }}
msgid "delete %(ma_act)s in %(cat_name)s"
msgstr "borrar esta acción de '%(ma_act)s' de la categoría '%(cat_name)s'"

# original is msgid save {{ ma_act }} in {{ cat_name }}
msgid "save %(ma_act)s in %(cat_name)s"
msgstr "guardar el texto introducido de esta acción de '%(ma_act)s' de la categoría '%(cat_name)s'"

# original is msgid text of {{ ma_act }} in {{ cat_name }}
msgid "text of %(ma_act)s in %(cat_name)s"
msgstr "texto de la acción de '%(ma_act)s' de la categoría '%(cat_name)s'"

# original is msgid ({{ ma_act }} in {{ cat_name }})
msgid "(%(ma_act)s in %(cat_name)s)"
msgstr "(introduzca el texto de la nueva acción de '%(ma_act)s' de la categoría '%(cat_name)s')"

#. Translators: used in memberannouncement_filters.html
msgid "show announcements of all languages"
msgstr "mostrar los intercambios de todas las idiomas"

msgid "show announcements of your currently selected language"
msgstr "mostrar solo los intercambios del idioma seleccionado"

msgid "show the announcements of all members"
msgstr "mostrar los intercambios de todos los miembros"

msgid "only show your announcements"
msgstr "solo mostrar los intercambios que has creado"

msgid "enter search text fragment to filter announcements"
msgstr "mostrar solo los intercambios que contienen el texto introducido"

#. Translators: used in membermessage_plugin.html
msgid "add a new message"
msgstr "añadir un nuevo mensaje"

#. Translators: used in membermessage_edit.html
msgid "(enter new member message text)"
msgstr "(introduzca el texto de esta mensaje nueva)"

msgid "check to show this message publicly to any visitor of this website"
msgstr "marcar este casillo para mostrar esta mensaje a cada visitante de este sitio de web"
" o desmarcarlo si quires que esta mensaje sea solo visible para los miembros"

msgid "enter the date until this message will be visible in the website news"
msgstr "introduzca el día hasta que esta mensaje va ser visible en la area de noticias recientes"
" o deja este campo vacío para nunca ocultar esta mensaje"

msgid "delete this member message"
msgstr "borrar esta mensaje"

msgid "save any changes done to this member message"
msgstr "guardar los cambios hechos a esta mensaje"

#. Translators: used in mbr_announcements/views.py
msgid "announcement"
msgstr "anuncio"

msgid "{ma_cat_name}-{ma_action}-announcement"
msgstr "{ma_action}-{ma_cat_name}-anuncio"

#. Translators: used in mbr_messages/views.py
msgid "private message"
msgstr "mensaje interno"

msgid "public message"
msgstr "mensaje publico"

msgid "expiring {new_expired}"
msgstr "visible hasta {new_expired}"

msgid "message"
msgstr "mensaje"

msgid "made public"
msgstr "publicado"

msgid "revoked publication"
msgstr "publicación revocada"

msgid "added expiry date {new_expired}"
msgstr "añadida fecha de vencimiento {new_expired}"

msgid "removed expiry date {old_expired}"
msgstr "eliminado la fecha de vencimiento {old_expired}"

msgid "changed expiry date from {old_expired} to {new_expired}"
msgstr "modificado la fecha de vencimiento de {old_expired} a {new_expired}"

#. Translators: for member/admin notifications, used in kairos/utils.py and membermeetings_plugin.html
msgid "Notify members"
msgstr "Notificar a los miembros"

msgid "Update and/or Notify"
msgstr "Actualizar y/o Notificar"

msgid "notification from kairos website"
msgstr "notificación del sitio web de kairos"

msgid "by {admin}"
msgstr "por {admin}"

msgid "<a href=\"{url}\">{item}</a> of {owner} got added"
msgstr "<a href=\"{url}\">{item}</a> de {owner} se ha añadido"

msgid "<a href=\"{url}\">{item}</a> of {owner} got changed"
msgstr "<a href=\"{url}\">{item}</a> de {owner} se ha cambiado"

msgid "<i>{item}</i> of {owner} got deleted"
msgstr "<i>{item}</i> de {owner} se ha borrado"

msgid "<i>Member Meeting</i> notification"
msgstr "Notificación de <i>Reunion de Miembros</i>"
