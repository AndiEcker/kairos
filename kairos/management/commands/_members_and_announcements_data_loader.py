""" reset the member names/ids, announcement categories and offers/requests from a file and the old web/html.

execute from the project root (src/kairos) as the cwd.
"""
import datetime
import os
import re
import requests

from typing import Optional

# noinspection PyUnresolvedReferences
from bs4 import BeautifulSoup, PageElement

from django.contrib.auth import get_user_model
from django.core.files import File

from members_upserter import load_members, upsert_members

# import the following modules after members_upserter to ensure proper setup of django settings environment!
from filer.models import Image
from filer.management.commands.import_files import FileImporter
from mbr_announcements.models import AnnounceCategory, MemberAnnouncement


assert os.getcwd() == "False"   # prevent that this script get executed accidentally (assert False shows inspections)


ACTION_NAMES = {"Biete": "Offer", "Suche": "Request", "Verleih": "Rent"}
CATEGORY_NAMES = {"Alltagshilfe": ("Everyday help", "Ayuda diaria"),
                  "Betreuung": ("Care", "Cuidados"),
                  "Coaching": ("Coaching", "Entrenamiento"),
                  "Gartenarbeit": ("Gardening", "Jardinería"),
                  "Handwerk": ("Handicraft", "Artesanía"),
                  "Lebensmittel": ("Foodstuffs", "Alimentos"),
                  "Malen": ("Painting", "Pintar"),
                  "Massage": ("Massage", "Masaje"),
                  "Sprachen": ("Languages", "Idiomas"),
                  "Tanzen": ("Dancing", "Bailando"),
                  "Wandern": ("Hiking", "Senderismo"),
                  }

# re to parse/strip member id, name and desc; two hyphen chars: &#8211; (176 matches) and normal hyphen (319 matches)
ID_NAME_DESC_RE = re.compile(r"^(\d+)\s*[\s\-–]\s*(\w+)[\s.]*:[\s\-–]*([\s\S]*)\s*$")

MEDIA_INI_PATH = "media_ini"


# read 154 members ####################################################################################################
with open(os.environ['MEMBERS_DATA_FILE_PATH'], newline="") as file_handle:
    field_names, members = load_members(file_handle, print_fn=print)
upsert_members(field_names, members, reset=False, force=False, verbosity=3, print_fn=print)

# get the latest announcement data from old website ###################################################################
html_page = requests.get('https://kairos-gomera.org/angebote/')

soup_elements = BeautifulSoup(html_page.text, 'html.parser')
body_elements = soup_elements.body
data_elements = body_elements.find("div", "entry-content")

cat_name = "Entspannung"                    # html-fix: the "Entspannung" category has no h5 element (instead is a <p>)
fix_h5 = data_elements.find('p', string=cat_name)
h5_tag = soup_elements.new_tag('h5', sourceline=fix_h5.sourceline)
h5_tag.string = cat_name
fix_h5.replace_with(h5_tag)

pretty_html = data_elements.prettify()      # get different sourceline for nested div/p elements in the same row/line
with open(f"{os.environ['ANNOUNCEMENTS_PRETTY_PATH_PREFIX']}{datetime.date.today()}.txt", 'w') as fp:
    fp.write(pretty_html)

pretty_elements = BeautifulSoup(pretty_html, 'html.parser')


# parse and import the 26 announcement categories (offers and requests) ###############################################
def filer_image_field_value(file_name):
    """ convert file name to filer image field value """
    file_content = File(open(f"{MEDIA_INI_PATH}/{file_name}", 'rb'))
    img = Image.objects.create(original_filename=file_name, file=file_content)
    return img


categories = []
headings = pretty_elements.find_all("h5")
for cat_element in headings[26:]:           # with 26 skip links in header and "Neue Einträge"
    cat_name = cat_element.string.strip()
    if cat_name == 'Sport':
        img_file = "eci_sport.jpg"
    else:
        find_func = cat_element.find_previous if cat_name == "Diverses" else cat_element.find_next
        img_url = find_func('img').attrs['src']
        img_ext = img_url.rsplit('.')[-1]
        img_file = f"eci_{cat_name.lower()}.{img_ext}"
        response = requests.get(img_url)
        if response.status_code == 200:
            with open(f"{MEDIA_INI_PATH}/{img_file}", 'wb') as fp:
                fp.write(response.content)
    categories.append((cat_name, img_file, cat_element))

# upload all files in MEDIA_INI_PATH to the filer storage (page images, pdf-form and the just downloaded category icons)
# WARNING: will duplicate files; delete() is throwing exception 'File' object has no attribute 'file_ptr', therefore
# .. before you run this, delete first all the uploaded files manually within admin/filer
# from filer.models.filemodels import File
# File.objects.all().delete()
FileImporter(path=MEDIA_INI_PATH, verbosity=3).walker()

AnnounceCategory.objects.all().delete()
for category in categories:
    cat_name, img_file, _cat_element = category
    row = AnnounceCategory.objects.create(ac_name=cat_name, ac_image=filer_image_field_value(img_file))
    row.save()
    print(f"created category {cat_name} with image file {img_file}")
    if cat_name in CATEGORY_NAMES and cat_name not in CATEGORY_NAMES[cat_name]:
        for foreign_cat in CATEGORY_NAMES[cat_name]:
            row = AnnounceCategory.objects.create(ac_name=foreign_cat, ac_image=filer_image_field_value(img_file))
            row.save()
            print(f"+foreignCategory {foreign_cat}")


# parse and import the 448 announcement-rentals/-offers/-requests #####################################################
def next_entry(tag: PageElement) -> Optional[PageElement]:
    """ find next announcement-action entry """
    nxt_div = tag.find_next('div')
    while nxt_div and nxt_div.find('h5'):   # skip category heading
        nxt_div = nxt_div.find_next('div')
    while nxt_div:                          # fix div wrong structured, adding: 3 - Egon: Malunterricht Öl, Acryl...
        nd = nxt_div.find('div')
        if not nd:
            break
        nxt_div = nd

    nxt_p = tag.find_next('p')
    if nxt_p and nxt_div.sourceline < nxt_p.sourceline and not nxt_div.find('p') and not nxt_div.find('div'):
        nxt_p = nxt_div

    return nxt_p or nxt_div                 # or nxt_div for to find/add last entry: 136 - Monika: Begegnung auf...


def _clean_desc(desc: str) -> str:
    assert "\r" not in desc, f"r in {desc}"
    assert "\t" not in desc, f"t in {desc}"
    lines = desc.split(os.linesep)
    clean = ""
    for line in lines:
        if line[:14].strip():
            clean += line + os.linesep
        else:
            clean += line.lstrip() + os.linesep
    return clean[:-1].replace(os.linesep * 2, os.linesep).replace(os.linesep * 2, os.linesep)


announcements = []
ma_act = usr_nam = usr_first_name = ''
for cat_idx, (cat_name, cat_line, cat_element) in enumerate(categories):
    next_tag = next_entry(cat_element)
    # noinspection PyUnresolvedReferences
    while next_tag and (cat_idx >= len(categories) - 1 or next_tag.sourceline <= categories[cat_idx + 1][2].sourceline):
        next_txt = next_tag.text            # .get_text(strip=True) also removes inner formatting (e.g. \n, spaces)
        if next_txt:
            next_txt = next_txt.replace(" ", " ").replace("./.", "").strip()
            if next_txt[:-1] in ACTION_NAMES:
                ma_act = next_txt[:-1]
            elif next_txt:
                found = ID_NAME_DESC_RE.match(next_txt)
                if found:
                    usr_nam, usr_first_name, ma_desc = found.group(1, 2, 3)
                    # assert usr_nam and usr_first_name and ma_act, f"{next_txt}: {usr_nam}|{usr_first_name}|{ma_act}"
                    # assert ma_desc == ma_desc.strip()
                    announcements.append([usr_nam, ma_act, cat_name, _clean_desc(ma_desc)])
                else:                       # extra paragraph, to be added to the desc string of the last entry
                    announcements[-1][3] += os.linesep + _clean_desc(next_txt)
        next_tag = next_entry(next_tag)

usr_model = get_user_model()
MemberAnnouncement.objects.all().delete()
for announcement in announcements:
    usr_nam, ma_act, ma_cat, ma_desc = announcement
    row = MemberAnnouncement.objects.create(
        ma_user=usr_model.objects.get(last_name=usr_nam),
        ma_announce_category=AnnounceCategory.objects.get(ac_name=ma_cat),
        ma_language='de',
        ma_action=ma_act,
        ma_description=ma_desc,
    )
    row.save()
    print(f"created member announcement: {usr_nam} {ma_act} {ma_cat} {ma_desc}")
    if usr_nam in ('19', '35'):             # 19-Petra and 35-Lieve are having all offers/requests in english
        row = MemberAnnouncement.objects.create(
            ma_user=usr_model.objects.get(last_name=usr_nam),
            ma_announce_category=AnnounceCategory.objects.get(ac_name=CATEGORY_NAMES[ma_cat][0]),
            ma_language='en',
            ma_action=ACTION_NAMES[ma_act],
            ma_description=ma_desc,
        )
        row.save()
        print(f"+english member announcement: {usr_nam} {ACTION_NAMES[ma_act]} {CATEGORY_NAMES[ma_cat][0]} {ma_desc}")


print(f"{len(members)} members, {len(categories)} categories and {len(announcements)} offers/requests got reset.")
