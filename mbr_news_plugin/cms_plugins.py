""" plugin to display member news. """
import datetime

from cms.plugin_base import CMSPluginBase
from cms.plugin_pool import plugin_pool
from cms.models.pluginmodel import CMSPlugin
from django.db.models import CharField, F, Q, Value

from mbr_announcements.models import MemberAnnouncement
from mbr_announcements.views import EXC_ADD_MARKER, EXC_ANCHOR_ID_PREFIX
from mbr_messages.models import MemberMessage
from mbr_messages.views import MSG_ADD_MARKER, MSG_ANCHOR_ID_PREFIX


@plugin_pool.register_plugin
class MemberNewsPlugin(CMSPluginBase):
    """ display the newest member announcements and messages. """
    model = CMSPlugin
    render_template = "mbr_news_plugin.html"
    cache = False

    # using this plugin within the ckeditor/TextPlugin is only working in admin edit mode, but is not even calling
    # render() in the published page (and therefore not included in the published page) - same for kairos/cms_plugins.py
    # text_enabled = True

    def render(self, context, instance, placeholder):
        """ add object_list to context """
        context = super().render(context, instance, placeholder)

        mes = MemberAnnouncement.objects.filter(~Q(ma_description__contains=EXC_ADD_MARKER))  # filter(~Q.. == exclude(Q
        mes = mes \
            .annotate(news_id=F('pk'), member_pk=F('ma_user__pk'), phone=F('ma_user__last_name'),
                      news_date=F('ma_last_updated'), description=F('ma_description'),
                      cat_name=F('ma_announce_category__ac_name')) \
            .values('news_id', 'member_pk', 'phone', 'news_date', 'description', 'cat_name')

        mms = MemberMessage.objects \
            .filter(Q(mm_expired__gt=datetime.date.today()) | Q(mm_expired__isnull=True)) \
            .exclude(Q(mm_text__contains=MSG_ADD_MARKER))
        if not context.get('user').is_authenticated:
            mms = mms.filter(mm_public=True)
        mms = mms \
            .annotate(member_pk=F('mm_author__pk'), cat_name=Value("", output_field=CharField())) \
            .values('pk', 'mm_author__pk', 'mm_author__last_name', 'mm_created', 'mm_text', 'cat_name')

        context['object_list'] = mes.union(mms).order_by('-news_date')[:9]
        context['EXC_ANCHOR_ID_PREFIX'] = EXC_ANCHOR_ID_PREFIX
        context['MSG_ANCHOR_ID_PREFIX'] = MSG_ANCHOR_ID_PREFIX

        return context
