""" member messages views """
import datetime
import re

from urllib.parse import urlparse

from cms.utils.permissions import get_current_user
from django.contrib.auth.models import User
from django.http import HttpResponseRedirect
from django.urls import reverse
from django.utils.translation import gettext as _
from django.views.generic import ListView

from ae.django_utils import set_url_part

from kairos.utils import send_mbr_notif
from .models import MemberMessage


MSG_ADD_MARKER = "EmptyMemberMessage"
MSG_ANCHOR_ID_PREFIX = "MMsgAIdP"

MOBILE_AGENT_RE = re.compile(r".*(iphone|mobile|androidtouch)", re.IGNORECASE)


def check_change_notification(old_public: bool, old_expired: datetime.date, old_text: str,
                              new_public: bool, new_expired: datetime.date, new_text: str,
                              author: User, url: str):
    """ check for changes and if yes then send notifications to receivers.

    :param old_public:          old publication status of message.
    :param old_expired:         old message expiry date.
    :param old_text:            old message text.
    :param new_public:          new publication status.
    :param new_expired:         new expire date.
    :param new_text:            new message text.
    :param author:              message author user record.
    :param url:                 absolute url to show the changed message on the website.
    """
    added = MSG_ADD_MARKER in old_text
    changes = []
    if added:
        item = _("public message") if new_public else _("private message")
        if new_expired:
            item += " " + _("expiring {new_expired}").format(new_expired=new_expired)
        changes.append(new_text)
    else:
        item = _("message")
        prefix = "- "

        if old_public != new_public:
            changes.append(prefix + (_("made public") if new_public else _("revoked publication")))

        if old_expired != new_expired:
            if not old_expired:
                changes.append(prefix + _("added expiry date {new_expired}").format(new_expired=new_expired))
            elif not new_expired:
                changes.append(prefix + _("removed expiry date {old_expired}").format(old_expired=old_expired))
            else:
                changes.append(prefix + _("changed expiry date from {old_expired} to {new_expired}").format(
                    old_expired=old_expired, new_expired=new_expired))

        if old_text != new_text:
            changes.append(new_text)

    if changes:
        send_mbr_notif('added' if added else 'updated', changes, item, author, url)


def extend_context(context, request, **kwargs):
    """ extend context for view and plugin (to avoid redundancies) """
    context['MSG_ADD_MARKER'] = MSG_ADD_MARKER
    context['MSG_ANCHOR_ID_PREFIX'] = MSG_ANCHOR_ID_PREFIX
    context['is_mobile'] = MOBILE_AGENT_RE.match(request.META['HTTP_USER_AGENT'])
    context['url_kwargs'] = kwargs


def get_queryset():
    """ get queryset of all objects for view and plugin (to avoid redundancies). """
    obj_list = MemberMessage.objects
    if not get_current_user().is_authenticated:
        obj_list = obj_list.filter(mm_public=True)
    return obj_list.order_by("-mm_created")


class MemberMessageListView(ListView):
    """ member messages list view """
    model = MemberMessage

    def get_context_data(self, **kwargs):
        """ context data """
        context = super().get_context_data(**kwargs)
        extend_context(context, self.request, **self.kwargs)
        return context

    def get_queryset(self):
        """ filter by currently selected language and order by category-name, action and newest changes first. """
        return get_queryset()

    def post(self, request, *args, **kwargs):
        """ handle member message edit. """
        mm_id: int = kwargs.get('id')
        if not mm_id:   # action == 'add'
            mm_author = get_current_user()
            row = MemberMessage.objects.create(
                mm_author=mm_author,
                mm_text=MSG_ADD_MARKER,
                mm_public=False,
            )
            mm_id = row.pk
            url = reverse('message-edit', kwargs={'id': mm_id})

        else:
            if request.path.split("/")[-2] == 'delete':
                rec = MemberMessage.objects.get(id=mm_id)
                pre_rec = self.get_queryset().filter(mm_created__lt=rec.mm_created).first()
                if pre_rec:
                    mm_id = pre_rec.pk
                    url = reverse('message-show', kwargs={'id': mm_id})
                else:
                    mm_id = 0
                    url = reverse('messages-list')
                if MSG_ADD_MARKER not in (txt := rec.mm_text):
                    send_mbr_notif('deleted', ([f"- {rec.mm_expired}"] if rec.mm_expired else []) + [txt],
                                   _("public message") if rec.mm_public else _("private message"), rec.mm_author)
                rec.delete()

            else:   # == 'update'
                rec = MemberMessage.objects.get(id=mm_id)
                old_public, old_expired, old_text, author = rec.mm_public, rec.mm_expired, rec.mm_text, rec.mm_author
                new_public, new_expired, new_text = (request.POST.get('mm_public') == 'on',
                                                     request.POST['mm_expired'] or None,
                                                     request.POST['mm_text'].strip())

                rec.mm_public, rec.mm_expired, rec.mm_text = new_public, new_expired, new_text
                rec.save()
                url = reverse('message-show', kwargs={'id': mm_id})

                check_change_notification(
                    old_public, old_expired, old_text,
                    new_public, new_expired, new_text,
                    author, set_url_part(request.build_absolute_uri(url), MSG_ANCHOR_ID_PREFIX + str(mm_id)))

        url = set_url_part(url, MSG_ANCHOR_ID_PREFIX + str(mm_id), urlparse(request.META.get('HTTP_REFERER')).query)

        return HttpResponseRedirect(url)
