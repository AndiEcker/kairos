""" register member messages model to be maintainable within django admin. """
from django.contrib import admin
from .models import MemberMessage

admin.site.register(MemberMessage)
