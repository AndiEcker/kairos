""" member messages app models """
from django.conf import settings
from django.db import models

from kairos.utils import member_full_name


class MemberMessage(models.Model):
    """ member messages model """
    mm_author = models.ForeignKey(settings.AUTH_USER_MODEL, on_delete=models.CASCADE)
    mm_text = models.TextField()
    mm_public = models.BooleanField()
    mm_expired = models.DateField(null=True, blank=True)
    mm_created = models.DateTimeField(auto_now_add=True)

    def __str__(self):
        # noinspection PyUnresolvedReferences
        return (f"{self.__class__.__name__}@{member_full_name(self.mm_author)}" +
                (f"->{self.mm_expired}" if self.mm_expired else "") +
                (" (public)" if self.mm_public else "") +
                f":{self.mm_text}")
