""" models for announcements between members """
from django.conf import settings
from django.db import models
from filer.fields.image import FilerImageField

from kairos.utils import member_full_name


class AnnounceCategory(models.Model):
    """ member announcement categories """
    ac_name = models.CharField(max_length=39)
    ac_image = FilerImageField(blank=True, null=True, on_delete=models.SET_NULL)

    class Meta:
        verbose_name_plural = "AnnounceCategories"
        constraints = (models.UniqueConstraint(fields=['ac_name'], name='unique_category_name'), )

    def __str__(self):
        return f"{self.__class__.__name__}/{self.ac_name}"


class MemberAnnouncement(models.Model):
    """ member announcements offered or requested/searched by members """
    ma_user = models.ForeignKey(settings.AUTH_USER_MODEL, on_delete=models.CASCADE)
    ma_announce_category = models.ForeignKey(AnnounceCategory, on_delete=models.CASCADE)
    ma_language = models.CharField(max_length=3, choices=settings.LANGUAGES)
    ma_action = models.CharField(max_length=12)     # e.g. offer/request/rent
    ma_description = models.TextField()
    ma_last_updated = models.DateTimeField(auto_now=True)    # needed for to determine the latest entries/changes

    class Meta:
        constraints = (models.UniqueConstraint(fields=['ma_user', 'ma_announce_category', 'ma_language', 'ma_action'],
                                               name='unique_user_category_language_action'), )

    def __str__(self):
        # noinspection PyUnresolvedReferences
        return f"{self.__class__.__name__}@{member_full_name(self.ma_user)}" \
               f"/{self.ma_announce_category.ac_name}_{self.ma_action}_{self.ma_language}"
