""" member announcements plugin """
from cms.models import CMSPlugin
from cms.plugin_base import CMSPluginBase
from cms.plugin_pool import plugin_pool

from mbr_announcements.views import extend_context, get_queryset


@plugin_pool.register_plugin
class MemberAnnouncementsPlugin(CMSPluginBase):
    """ member messages plug-in """
    model = CMSPlugin
    render_template = "memberannouncement_plugin.html"
    cache = False

    def render(self, context, instance, placeholder):
        """ add context (simulating AnnouncementsListView.get_context_data()/.get_queryset()) """
        context = super().render(context, instance, placeholder)
        context['object_list'] = get_queryset(context['request'])
        extend_context(context, context['request'])
        return context
