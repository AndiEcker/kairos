""" announcements views. """
import datetime

from urllib.parse import urlparse

from django.db.models import Q
from django.http import HttpResponseRedirect
from django.urls import reverse
from django.utils.text import slugify
from django.utils.translation import gettext as _, override
from django.views.generic import ListView

from cms.utils.permissions import get_current_user

from ae.django_utils import set_url_part, requested_language

from kairos.utils import send_mbr_notif
from .models import AnnounceCategory, MemberAnnouncement


EXC_CAT_ACT_SEP = '+c+a+'
EXC_ADD_MARKER = 'EmptyMemberAnnouncement'
EXC_ANCHOR_ID_PREFIX = "MAnnAIdP"


def check_change_notification(log_desc: str, rec: MemberAnnouncement, chk_desc: str = "", url: str = ""):
    """ check changes on MemberAnnouncement record and send notifications if yes.

    :param log_desc:            announcement description text to be logged/send-in-notification, which is on update the
                                new one and on deletion the actual one.
    :param rec:                 added/updated/deleted announcement record data (author, category and action).
    :param chk_desc:            old announcement description text on update or "" on deletion.
    :param url:                 absolute url to show the announcement ("" on deletion).
    """
    if log_desc != chk_desc:
        with override(rec.ma_language):
            item = _("{ma_cat_name}-{ma_action}-announcement").format(ma_cat_name=rec.ma_announce_category.ac_name,
                                                                      ma_action=rec.ma_action)
            mode = 'deleted' if chk_desc == "" else 'added' if EXC_ADD_MARKER in chk_desc else 'updated'
            send_mbr_notif(mode, [log_desc], item, rec.ma_user, url)


def extend_context(context, request, **kwargs):
    """ extend context for view and plugin (bundled here to avoid redundancies) """
    context['EXC_ADD_MARKER'] = EXC_ADD_MARKER
    context['EXC_CAT_ACT_SEP'] = EXC_CAT_ACT_SEP
    context['EXC_ANCHOR_ID_PREFIX'] = EXC_ANCHOR_ID_PREFIX
    context['url_kwargs'] = kwargs

    context['user_cat_acts'] = user_cat_acts = []  # in order to prevent duplicate member announcements per user
    current_user = get_current_user()
    if current_user.is_authenticated:
        for ma_object in MemberAnnouncement.objects.filter(ma_user=current_user):
            user_cat_acts.append(ma_object.ma_announce_category.ac_name + EXC_CAT_ACT_SEP + ma_object.ma_action)


def get_queryset(request):
    """ get full/filtered queryset of announcements/objects for view and plugin, bundled here to avoid redundancies. """
    filters = {}
    search_txt = request.GET.get('q')
    if search_txt:
        filters.update(ma_description__contains=search_txt)
    if request.GET.get('l') == '1':
        filters.update(ma_language=requested_language())
    if request.GET.get('u') == '1':
        filters.update(ma_user=get_current_user())
    if filters:
        obj_list = MemberAnnouncement.objects.filter(Q(**filters) | Q(ma_description__contains=EXC_ADD_MARKER))
    else:
        obj_list = MemberAnnouncement.objects  # .all()

    return obj_list.order_by("ma_announce_category__ac_name", "ma_action", "-ma_last_updated")


class AnnouncementsListView(ListView):
    """ collapsible list of member announcement actions, plus inline editing, deleting and adding them. """
    model = MemberAnnouncement

    def get_context_data(self, **kwargs):
        """ context data """
        context = super().get_context_data(**kwargs)
        extend_context(context, self.request, **self.kwargs)
        return context

    def get_queryset(self):
        """ filter by currently selected language and order by category-name, action and newest changes first. """
        return get_queryset(self.request)

    def post(self, request, *args, **kwargs):
        """ handle member announcements add, delete and change of description. """
        ma_id: int = kwargs.get('id')
        if not ma_id:
            cat_name, ma_action, ma_user = kwargs['cat'], kwargs['act'], get_current_user()
            row = MemberAnnouncement.objects.create(
                ma_user=ma_user,
                ma_announce_category=AnnounceCategory.objects.get(ac_name=cat_name),
                ma_language=MemberAnnouncement.objects.filter(ma_action=ma_action).first().ma_language,
                ma_action=ma_action,
                ma_description=f"{ma_user}@{datetime.datetime.now()}+{cat_name}/{ma_action}={EXC_ADD_MARKER}",
            )
            ma_id = row.pk
            url = reverse('announcement-edit', kwargs={'cat': slugify(cat_name), 'id': ma_id})

        else:
            cat_slug = kwargs['cat']
            if request.path.split("/")[-3] == 'delete':
                del_rec = MemberAnnouncement.objects.get(id=ma_id)
                pre_rec = self.get_queryset().filter(ma_last_updated__lt=del_rec.ma_last_updated).first()
                if pre_rec:
                    ma_id = pre_rec.pk
                    url = reverse('announcement-show', kwargs={'cat': cat_slug, 'id': ma_id})
                else:
                    ma_id = 0
                    url = reverse('announcements-list')
                if EXC_ADD_MARKER not in (desc := del_rec.ma_description) and desc.strip():
                    check_change_notification(desc, del_rec)
                del_rec.delete()

            else:  # update
                url = reverse('announcement-show', kwargs={'cat': cat_slug, 'id': ma_id})
                upd_rec = MemberAnnouncement.objects.get(id=ma_id)
                old_desc = upd_rec.ma_description       # old rec desc gets changed by .update(ma_description=...)
                new_desc = request.POST['ma_description'].strip()
                if new_desc:
                    upd_rec.ma_description = new_desc
                    upd_rec.save()
                    check_change_notification(
                        new_desc, upd_rec,
                        old_desc, set_url_part(request.build_absolute_uri(url), EXC_ANCHOR_ID_PREFIX + str(ma_id)))

        url = set_url_part(url, EXC_ANCHOR_ID_PREFIX + str(ma_id), urlparse(request.META.get('HTTP_REFERER')).query)

        return HttpResponseRedirect(url)
