************************************************************************
OUTDATED - migrated to [Read The Docs](https://kairos.readthedocs.io)
************************************************************************

features and use-cases
**********************

this open source website project can be freely used and adapted to any exchange or sharing association or platform.

on the resulting website, association members can announce their exchange offers and requests, as well as post internal
or public messages.

not only the members and their data, but also all the pages structure and content of this website are freely designable
and maintainable by association members with special administration permissions. all permissions are configurable
individually for each member.

ids, names and phone numbers of members are only visible for authenticated members.
visitors of this website can see only the announcements texts and explicitly published messages.

depending on your role you find more information in the :ref:`member manual`, the :ref:`administrator manual` or the
:ref:`programmer manual` of this website project.


member manual
*************


member account actions
======================

as a member you can sign or log into this website by clicking or tapping on the log-/sign-in button with the icon shown
underneath:

.. figure:: img/login_button.png
           :alt: button to open the form to log-/sign-in as association member
           :scale: 180 %
           :target: img/login_button.png

this sign-/log-in button is situated at the top of each page in the navigation menu, on the top right. on mobile devices
(and devices with small screens) this button gets hidden within the ``burger menu``, which can be opened by
tapping on the button with the three horizontal lines.

after clicking or tapping on the above log-in button the following form gets displayed to authenticate you as a member
your member id (entered in the username field) and password:

.. figure:: img/login_form.png
           :alt: login form to authenticate as member of the association
           :scale: 120 %
           :target: img/login_form.png

after you entered your credentials and hit the `Log in` button you will get authenticated and recognized as a member,
which provides additional functionality to you, depending on the permissions and rights you got granted.

as an authenticated member you now see the following two buttons, at the place were the log-in button was situated (at
the top right of the navigation bar):

.. figure:: img/navbar_member_account_actions.png
           :alt: buttons to logout or to change the password of an authenticated member
           :scale: 180 %
           :target: img/navbar_member_account_actions.png

with the left one you can change your password. hitting/tapping the right one is logging/signing you out from this
website.


member language
===============

this project is by default supporting the member languages ``english``, ``spanish`` and ``german``.

visitors and members can switch the current language with the following dropdown buttons, situated at the top left of
the navigation bar of this website:

.. figure:: img/navbar_language_selector.png
           :alt: navigation bar buttons to change/switch the website language
           :scale: 180 %
           :target: img/navbar_language_selector.png


member announces
================

all announcements are grouped into categories, like for example `Care`, `Coaching`, `Everyday Help` or `Handicraft`.

another sub-group level is grouping the announcements within each category. this sub-groups are called category actions.
commonly available actions are e.g. `Request`, `Offer` or `Rentals`.

the button at the top left of the following picture, labeled with `Care`, is (together with the category image on their
right) representing the announcement category. by hitting this button the announcements within this category can
be collapsed/hided (if they were shown) or expanded/shown (if they were hidden before):

.. figure:: img/announcement_category_and_action.png
           :alt: announcement category structure with buttons to expand/collapse and to add a new announcement
           :scale: 99 %
           :target: img/announcement_category_and_action.png

the announcement action (in the above example `Offer`) gets displayed underneath the category button.


announcement text edit controls
-------------------------------

an authenticated member can add, update and delete rental, offer and request announcements directly on the website.

a new announcement text can be created with the `Add` button displayed to the right of a announcement category action:

.. figure:: img/announcement_add_button.png
           :alt: button to add a new announcement into the respective category and action
           :scale: 120 %
           :target: img/announcement_add_button.png

the language of a new announcement is associated to the category action under which it got created (and is therefore
independent from the currently selected website language).

the text of an announcement can be entered or edited within the following form:

.. figure:: img/announcement_edit_form.png
           :alt: form to edit or delete an announcement entry (offer/request/rental/...)
           :scale: 90 %
           :target: img/announcement_edit_form.png

hit the `Save` button to add or edit/update the announcement text, or the `Delete` button to remove the announcement
from the website.

a member can create exactly one entry under each announcement category action.


announcement display controls
-----------------------------

visitors can filter the announcements by language and/or by an entered search text fragment.
you as an authenticated members can additionally filter the announcements to only display those that you have created.

the picture underneath is showing the controls of the three provided filters, which are arranged in two rows:

.. figure:: img/announcement_filter_controls.png
           :alt: controls to filter announcements by language, member or a search text
           :scale: 120 %
           :target: img/announcement_filter_controls.png

the first row consists of two filters, one on the left and another one on the right, implemented as radio button groups
with two icons each. the currently chosen radio button icon is shown bigger and with a golden glow. tap or click on the
smaller displayed icon to toggle the radio buttons and the respective filter.

with the radio buttons on the left of the first row you can filter the announcements by language. by default the
announcements of all languages will be displayed. to only display announcements of the currently chosen language click
on the smaller country flag:

.. figure:: img/announcement_language_filter.png
           :alt: radio buttons to filter announcements by language
           :scale: 180 %
           :target: img/announcement_language_filter.png

to only display the announcements entered by yourself, click on the left button of the radio button group shown on the
right of the first row:

.. figure:: img/announcement_member_filter.png
           :alt: radio buttons to filter announcements introduced by the authenticated member
           :scale: 180 %
           :target: img/announcement_member_filter.png

enter a search text into the field of the second row to display only the announcements that are containing the entered
text fragment.

the following three buttons, shown at the bottom of all announcements, allow to expand or collapse the announcement
categories all together:

.. figure:: img/announcement_categories_collapse.png
           :alt: controls to expand or collapse the announcement categories
           :scale: 120 %
           :target: img/announcement_categories_collapse.png

a click on left button will expand all announcement categories. to collapse all categories click on the button on the
right. the middle button is toggling the collapse state, so that all the expanded categories will be collapsed and the
collapsed categories will be expanded.


member messages
===============

you as an authenticated members can add or edit your own messages directly on the website.

to create a new message press/tap the `Add Another` button:

.. figure:: img/message_add_button.png
           :alt: button to add a new member message
           :scale: 180 %
           :target: img/message_add_button.png

after hitting this button the following form gets displayed, which is also used to edit or delete a member message:

.. figure:: img/message_edit_form.png
           :alt: input form to add, edit or delete a member message
           :scale: 120 %
           :target: img/message_edit_form.png

to edit a member message on a desktop computer double click on the message text. on mobile phones the message can be
edited by either double-tap on the message text or after clicking the `Edit` button.

by checking the `is published` entry field a message can be classified as `public`. leaving this checkbox unchecked
results in a `private` message. private messages are only visible for members, whereas public message are visible for
every visitor of your association website.

a message can optionally have an `expire date`; if the current day's date lies after the entered expiry date, then this
message will no longer be shown in the news box.

new and edited member messages get also displayed within the news box (most recent ones first).

visitors will see only the message texts, whereas authenticated members see also the id, name and phone number of the
member who wrote the message.


news box
========

the scrollable news box (mostly placed at the top of the home CMS page) displays the nine most recent
news of your association members (the very newest first). this includes newly added or edited and not expired member
messages and any newly added or updated announcement.

the following handle icon, situated at the bottom right corner of the news box, allows to resize it vertically:

.. figure:: img/news_box_resizing.png
           :alt: handle to resize the news box vertically
           :scale: 180 %
           :target: img/news_box_resizing.png

tap on any news text within the news box to jump to another page to display more details for the represented member
announcement or message.

authenticated members are also seeing the id and the name of the member which entered the announcement/message. on
desktop computers place your mouse pointer on the displayed ``id-name`` text to show the phone number of the member. on
mobile phones you can even tap on the displayed ``id-name`` text to initiate a phone call to this member.


members meeting box
===================

this plug-in can be placed in any content/CMS page to display a small text with information on the next planned
members meeting in the language selected by the user in the browser.

a authenticated member with the ``add members meetings`` permission will see this plug-in in edit mode with a text area
control and a checkbox for each supported language:

.. figure:: img/members_meeting_box_edit_mode.png
           :alt: members meeting box in edit mode
           :scale: 90 %
           :target: img/members_meeting_box_edit_mode.png

to highlight parts of the information text a few html tags (like b, a, i, pre or code) are supported (see
the doc-string of the method :meth:`ae.notify.Notifications.send_telegram`).

all languages can be edited and saved with the button at the bottom of this plug-in. tick the checkbox to send a
notification message to the messenger group of your association in the respective language.


administrator manual
********************

website administrators are members with additional rights granted to maintain:

    * members data
    * pages content, menus and tooltips
    * announcement categories, actions and any requests/offers
    * member messages
    * member meetings

as an administrators you are using an extra account to sign-in to get access to the
`Django admin toolbar and menus <https://docs.djangoproject.com/en/dev/ref/contrib/admin/>`_. the admin toolbar gets
then displayed at the top of your browser window as a fixed overlay:

.. figure:: img/admin_toolbar.png
           :alt: configure administrator language
           :scale: 180 %
           :target: img/admin_toolbar.png

the most important menu in the admin toolbar is the site menu (by default titled as `kairos-gomera.org`):

.. figure:: img/admin_site_menu.png
           :alt: toolbar site menu
           :scale: 180 %
           :target: img/admin_site_menu.png

the toolbar language can be configured via the `User settings...` menu item, available from the site menu:

.. figure:: img/admin_language_setting.png
           :alt: configure administrator language
           :scale: 180 %
           :target: img/admin_language_setting.png

most administration tasks can be done via the `Administration...` menu item of the site menu. for more details check
out the related documentation of `Django <https://docs.djangoproject.com/en/dev/ref/contrib/admin/>`_ and the
`Django CMS <https://djangocms.readthedocs.io/en/latest/user/reference/page_admin/>`_.


member administration
=====================

the id, name, email and phone number of a member of your association can be maintained within the two available `Users`
forms, via the `Administration...` menu item of the site menu. the second `Users (page)` form of the `DJANGO CMS`
admin group has additional fields used for administrators, which are not available in the first `Users` form under the
``AUTHENTICATION AND AUTHORIZATION` admin group.

the member id gets stored in the `USERNAME` field and the member's phone number in the `LAST NAME` field.

see the :ref:`programmer manual` on how to bulk load member data into your website.


member permissions
------------------

a separate user account has to be created for members with an admin role, to manage the website pages and their content.
only these admin accounts should have the `is_staff` field of the user record set to True. the username field should
contain the first name of the member (instead of a member id).

adding a member to the `members` user group results in the following permissions:

* `perms.mbr_announcements.add_memberannouncement`: add an announcement.
* `perms.mbr_announcements.change_memberannouncement`: edit an announcement.
* `perms.mbr_announcements.delete_memberannouncement`: delete an announcement.
* `perms.mbr_messages.add_membermessage`: add a message.
* `perms.mbr_messages.change_membermessage`: change a message.
* `perms.mbr_messages.delete_membermessage`: delete a message.

individual member accounts can have the following additional admin permissions:

* `perms.auth.add_user` (auth | users | add): collect the admission fees in the sign-up process for new members.
* `perms.mbr_meeting_plugin.add_membermeeting` (mbr_meeting_plugin | member meeting | add): maintain the information
  shown by the ``members meeting box`` plugin regarding the next association meeting.


page content administration
===========================

the website pages and their contents are freely adaptable to the needs and rules of your association. their maintenance
can be done directly on this website by staff members with special administration permissions, and with the help of the
tools provided by the `Django CMS <https://django-cms.org/>`_.

all the pages available in your website are maintainable via the `Pages...` item of the site menu:

.. figure:: img/admin_pages.png
           :alt: pages menus and content in all languages
           :scale: 90 %
           :target: img/admin_pages.png

a page gets configured by its page settings and the content. check out the `page admin documentation of the Django CMS
<https://djangocms.readthedocs.io/en/latest/user/reference/page_admin/>`_ for more information.


default pages and reverse ids
-----------------------------

the following default CMS pages are already created but can be deleted or extended to adopt the website to your
association.

.. list-table:: Title
    :widths: 15 15 21 21 21
    :header-rows: 1

    * - reverse id
      - slug (path)
      - Menü-name
      - menu title
      - nombre de menu
    * - 0
      - home (/)
      - Startseite
      - Home
      - Inicio
    * - 10
      - about
      - Über uns
      - About us
      - Sobre nosotros
    * - 20
      - rules
      - Regeln
      - Rules
      - Reglas
    * - 60
      - philosophy
      - Philosophie
      - Philosophy
      - Filosofía
    * - 70
      - announcements
      - Inserate
      - Announcements
      - Anuncios
    * - 90
      - messages
      - Mitteilungen
      - Messages
      - Mensajes
    * - 120
      - mbr-list (members/list)
      - Mitglieder
      - Members
      - Miembros
    * - data_collection
      - data-collection
      - Datenerhebung
      - Data Collection
      - recogida de datos
    * - legal_notice
      - legal-notice
      - Impressum
      - Legal Notice
      - Aviso Legal
    * - mbr_signup
      - mbr-signup (signup/member)
      - Werde Mitglied
      - Member Signup
      - Hacerte Socio
    * - privacy_policy
      - privacy-policy
      - Datenschutzerklärung
      - Privacy policy
      - Política de privacidad


page content plug-ins
---------------------

the data added by your association members by publishing message and announcements get stored
in the integrated database of this website. the following CMS plug-ins can be integrated at any page of your website
to display this data and allow your members an easy maintenance of this data:

    * ``member announcements`` - to display and search for announcements, and allowing members to add, edit
      and delete announcements.
    * ``member messages`` - to display messages, and allowing members to add, edit and delete messages.
    * ``news box`` - to display the most recent member messages as well as the most recent announcements.
    * ``members meeting box`` - to display the next members meeting, and allowing members with special permissions to
      edit the meetings dates and invitations texts for each supported language.
    * ``page link`` - to place a cross reference to any other page of your website.

the ids, names and phone numbers of members, shown by these plug-ins, are only visible for authenticated members.
visitors of this website can see only explicitly published announcement/message texts.


announcement categories and action administration
=================================================

announcement categories and actions are created and maintained within the two edit forms of the
`Django admin site/area <https://docs.djangoproject.com/en/dev/ref/contrib/admin/>`_:

.. figure:: img/admin_announcements.png
           :alt: pages menus and content in all languages
           :scale: 90 %
           :target: img/admin_announcements.png

the first edit form (`AnnounceCategories`) allows you to add or change all announcement categories. the data of each
category consists of a category name and a category picture.

with the second edit form (`Member announcements`) you can edit the announcements created by your members. also the
creation of the first announcement item of a new announcement category or announcement action has to be done here.


Django CMS documentation for administrators
===========================================

* `Django CMS vs WordPress <https://www.django-cms.org/en/blog/2021/06/10/django-cms-vs-wordpress/>`_
* `Release Preview: django CMS 4.0 & Versioning (demo) <https://www.youtube.com/watch?v=72SficeO9N4>`_
* `Divio Einführung in Django CMS - YouTube (schwizerdütsch) <https://www.youtube.com/watch?v=ba_1jS4tS6w>`_
* `Django CMS Bedienungsanleitung für Redakteure <https://www.uni-augsburg.de/de/organisation/einrichtungen/rz/it-services/beschaeftigte/webseiten/bedienungsanleitung/>`_
* `Django CMS: Das flexible Python-System <https://cmsstash.de/cms-reviews/django-cms>`_


programmer manual
*****************

this section describes how to adapt this website project to the needs of your association.


bulk load of member and page content
====================================

as programmer you can adapt the python script ``_members_and_announcements_data_loader.py`` stored under the project
path ``kairos/management/commands`` to migrate the page content and your members (including their announcements)
from your old website to your new one (maintained by this project).

another option to bulk load member data from a CSV file into this website provides the Django manage.py command
``members_upserter``. this command can also be used after the initial migration process, to add or join multiple new
members.


multi-language support
======================

this project is by default supporting the member languages ``english``, ``spanish`` and ``german``.

adding or removing languages, to adapt it to the needs of your association, can be done easily by changing the
`Django language settings <https://docs.djangoproject.com/en/dev/topics/i18n/>`_. for each additional language a
`GNU gettext message file <https://docs.djangoproject.com/en/dev/topics/i18n/translation/#message-files>`_ has to be
provided (stored in this project underneath the kairos/locale folder).

some examples of multi-language messages, that may be set for your association individually in each language, are:

* `"KairosBrandName"`: brand name of your association.
* `"Time exchange * Goods exchange * Rental exchange"`: sub title of the website.
* `"{member_id}-{name}"`: format and content of the display name, member name or full_name of each member.
* `"{ma_cat_name}-{ma_action}-announcement"`: format and content of the display name of an announcement.

don't forget to run the Django manage.py command ``compilemessages`` after adding or changing translation texts in one
of the message files.


configure member changes notification channels
==============================================

this website can send notification messages automatically to an email account or to a person or a group of a messenger
service (like Telegram or WhatsApp) when member has added, updated or deleted a announcement or message, or when a new
member sign-up on this website.

the configuration of the used channels, services and receivers for the announcements is done by the following
OS environment variables (with fallback values provided by a dotenv file named ``.env``):

* `MCN_RECEIVERS`: comma-separated list of your messenger groups and admin members in the format
  service:address=name, where name is either the name of the messenger group or the full_name of a member.
* `MCN_MAIL_URI`: email service, host, username and password in the format
  [service://][user[:password]@]mail_server_host[:mail_server_port]
* `MCN_MAIL_FROM`: email sender address
* `MCN_TG_TOK`: Telegram API token
* `MCN_WA_TOK`: WhatsApp API token
* `MCN_WA_FROM`: WhatsApp sender id

more detailed information on the content and format of these environment variables you find in the source code of
kairos/utils.py and :mod:`ae.notify` (by searching in the code base for the environment variable name).


configure field validations
===========================

the format of data fields and the corresponding validation error messages can be configured via the settings dict
FIELD_VALIDATOR_RE_MSG, where the key is the field name and the value is a tuple with the regular expression in the
first item and the error message in the second item.

the following fields are configurable for your association:

* `first_name`: the first name of a member in the User model. the regular expression has to prevent the comma, colon
  and equal characters (,:=) for the correct parsing of the notification and sign-up admin names/channels (configured in
  the `MCN_RECEIVERS` and `SIGNUP_ADMINS` environment variables.)
* `phone`: the phone number of a member, which gets stored in the User model field `last_name`.


configure sign-up admins
========================

a sign-up admin is collecting the admission fees from new members.

the member-full_names and notification channels of the sign-up admins are configured by the OS environment variable
`SIGNUP_ADMINS`, which contains a comma-separated list of all members with sign-up admin permissions in the format
service:address=member_full_name.


multi-site support
==================

the Django CMS allows to specify multiple sites.

within the sites forms, available from the `Administration...` menu item of the site menu, you configure the urls and
display names of the site(s) of your association(s).


database queries
================

the following queries are useful to check, migrate or repair the content of the Django CMS database.

CMS page integrity checks can be done with the following query:

.. code-block:: sql

    SELECT reverse_id, language, slug, path, cms_page.id as "PageId", node_id, title, menu_title
      FROM cms_title LEFT OUTER JOIN cms_page ON cms_title.page_id = cms_page.id
      ORDER BY reverse_id, language, slug, path


queries to rename a Django app and model:

.. code-block:: sql

    UPDATE django_content_type SET app_label='new_app_name' WHERE app_label='old_app_name';
    UPDATE django_migrations SET app='new_app_name' WHERE app='old_app_name';
    ALTER TABLE old_app_name_old_model_name RENAME TO new_app_name_new_model_name


Django and Django CMS documentation for programmers
===================================================

* `Django vs WordPress — Which is Better For Your Website? <https://kinsta.com/blog/django-vs-wordpress/>`_
* `Django CMS tutorial <https://www.django-cms.org/en/blog/2020/07/07/5-step-django-cms-tutorial-for-new-developers/>`_
* `Django CMS documentation <https://www.django-cms.org/en/>`_
* `Mozilla Django Tutorial <https://developer.mozilla.org/en-US/docs/Learn/Server-side/Django>`_
* `Django Girls Tutorial <https://tutorial.djangogirls.org/>`_
* `Getting started with Django CMS <https://www.youtube.com/watch?v=NbsRVfLCE1U>`_
* `Django's Fundament <https://www.heise.de/select/ct/2022/1/2132610550810420862>`_
