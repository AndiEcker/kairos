# THIS FILE IS EXCLUSIVELY MAINTAINED by the project aedev.tpl_project V0.3.20
# RTD-build is failing if the Sphinx version is not specified/pinned (RTD was using 1.8.5 by default).
sphinx==4.4.0
# using package name sphinx_rtd_theme with underscores raises Sphinx Extension error:
# .. Could not import extension sphinx_rtd_theme==1.0.0 (exception: No module named 'sphinx_rtd_theme==1')
sphinx-rtd-theme==1.0.0
sphinx_autodoc_typehints
sphinx_paramlinks
-r ../dev_requirements.txt
# docs/conf.py is importing aedev.setup_project
aedev_setup_project
