""" model of page link plugin """
from django.db import models
from cms.models.fields import PageField
from cms.models.pluginmodel import CMSPlugin
from filer.fields.image import FilerImageField


class PageLinkBoxModel(CMSPlugin):
    """ bootstrap container box with an image, a text and a link to a cms page. """
    page_link = PageField(on_delete=models.CASCADE)
    page_image = FilerImageField(blank=True, null=True, on_delete=models.SET_NULL)
