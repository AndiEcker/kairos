""" kairos website tests
"""
import importlib
import os

from ae.base import TESTS_FOLDER                # type: ignore
from ae.inspector import module_attr            # type: ignore


main_imp_name = "kairos"
main_module = importlib.import_module(main_imp_name)


def test_version():
    """ test existence of package version. """
    # noinspection PyUnresolvedReferences
    pkg_version = main_module.__version__
    assert pkg_version
    assert isinstance(pkg_version, str)
    assert pkg_version.count(".") == 2
    assert pkg_version == module_attr(main_imp_name, '__version__')


def test_docstring():
    """ test existence of package docstring. """
    pkg_docstring = main_module.__doc__
    assert pkg_docstring
    assert isinstance(pkg_docstring, str)
    assert pkg_docstring == module_attr(main_imp_name, '__doc__')


def test_tests_folder_exists():
    """ test existence of tests folder. """
    assert os.path.isdir(TESTS_FOLDER)
