# OUTDATED - migrated to [ae-group](https://gitlab.com/ae-group/kairos)


# Kairos La Gomera Website Project

this is a django CMS website project made for the
[Non-profit Money-less, Time, Goods and Rental Exchange Association in La Gomera / Canary Islands](
https://kairos-gomera.org/).

this website got initially created with WordPress/Elementor. later it got migrated with this project to Django CMS
using [JetBrains PyCharm](https://www.jetbrains.com/pycharm/), the fantastic and IMO world's best Python IDE.

[here you find some hints on the features of this website](
https://gitlab.com/AndiEcker/kairos/-/blob/develop/docs/features_and_examples.rst)

[how to contribute to this project is explained here](
https://gitlab.com/AndiEcker/kairos/-/blob/develop/CONTRIBUTING.rst)
